package com.example.intentapp

interface Statics {
    val EXTRA_TEXT: String
        get() = "android.intent.extra.TEXT"

    fun clearText (viewId: Int)
    fun initializeUI ()
    fun goToTheOtherActivity ()
}