package com.example.intentapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), View.OnClickListener, Statics {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeUI()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.mainActivityClearButton -> {
                clearText(R.id.mainActivityText)
            }
            R.id.mainActivitySendButton -> {
                goToTheOtherActivity()
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val extraText = data!!.getStringExtra(Intent.EXTRA_TEXT)
                if (extraText != null) {
                    val text = findViewById<TextView>(R.id.mainActivityText)
                    text.text = extraText
                }
            }
        }
    }

    override fun clearText(viewId: Int) {
        val text = findViewById<TextView>(viewId)
        text.text = ""
    }

    override fun initializeUI() {
        val clearButton:Button = findViewById<Button>(R.id.mainActivityClearButton)
        val sendButton:Button = findViewById<Button>(R.id.mainActivitySendButton)
        clearButton.setOnClickListener(this)
        sendButton.setOnClickListener(this)
    }

    override fun goToTheOtherActivity() {
        val text = findViewById<TextView>(R.id.mainActivityText)
        val extraText = text.text.toString()
        val secondIntent = Intent(this@MainActivity, SecondActivity::class.java);
        secondIntent.putExtra(EXTRA_TEXT, extraText)

        startActivityForResult (secondIntent, 1)
    }
}