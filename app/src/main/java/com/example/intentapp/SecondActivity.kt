package com.example.intentapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SecondActivity : AppCompatActivity(), View.OnClickListener, Statics {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)
        initializeUI()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.secondActivityClearButton -> {
                clearText(R.id.secondActivityText)
            }
            R.id.secondActivitySendButton -> {
                goToTheOtherActivity()
            }
        }
    }

    override fun clearText(viewId: Int) {
        val text = findViewById<TextView>(viewId)
        text.text = ""
    }

    override fun initializeUI() {

        val intent: Intent = intent
        val extraText = intent.getStringExtra(EXTRA_TEXT)
        if (extraText != null) {
            val text = findViewById<TextView>(R.id.secondActivityText)
            text.text = extraText
        }
        val clearButton: Button = findViewById<Button>(R.id.secondActivityClearButton)
        val sendButton: Button = findViewById<Button>(R.id.secondActivitySendButton)
        clearButton.setOnClickListener(this)
        sendButton.setOnClickListener(this)
    }

    override fun goToTheOtherActivity() {
        val text = findViewById<TextView>(R.id.secondActivityText)
        val extraText = text.text.toString()
        val mainIntent = Intent()

        mainIntent.putExtra(EXTRA_TEXT, extraText)
        setResult(Activity.RESULT_OK, mainIntent)
        finish()
    }

}